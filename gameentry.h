#ifndef _GAMEENTRY_H_
#define _GAMEENTRY_H_

#include <netinet/in.h>

#include "list.h"

class GameEntry : private ListEntry<GameEntry> {
public:
	GameEntry(int gameid, struct in_addr *addr, int port1, int port2);
	~GameEntry() {}

	int compare(const GameEntry* ge);
	int hash(int max);
	void update();

	int show(char* buf, int size);

	struct in_addr addr;
	int port1;
	int port2;
	int gameid;
	long modtime;

protected:
	GameEntry(const GameEntry& ge);
	GameEntry& operator=(const GameEntry& ge);
};

#endif // _GAMEENTRY_H_
