/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "mod_gamespy2.h"
#include "modhelper.h"
#include "logging.h"

static struct game_ports port_arr[] = {
	{  2302,  2302, ID_HALO },
	{  3455,  3455, ID_PK },
	{ 10481, 10482, ID_SWAT4 },
	{ 29900, 29910, ID_BF2 },
	{ 0, 0, 0 }
};

static const char scanmsg[] = { 0xFE, 0xFD, 0x00, 0xDE, 0xAD, 0xBE, 0xEF, 0xFF, 0x00, 0x00 };
static const char replyhead[] = { 0x00, 0xDE, 0xAD, 0xBE, 0xEF };

static const char search_hostport[] = "hostport";
static const char search_gamename[] = "gamename";

static const char reply_bf2[] = "battlefield2";

void ModGameSpy2::scan(MultiSock* msock)
{
	ModHelper::send(msock, port_arr, scanmsg, sizeof(scanmsg));
}

int ModGameSpy2::parse(NetPkt* pkt, GameList* glist)
{
	int gameid, pos1, pos2, port;

	gameid = ModHelper::checkPorts(pkt, port_arr);
	if (!gameid)
		return PARSE_REJECT;

	if (!pkt->compare(0, replyhead, sizeof(replyhead)))
		return PARSE_REJECT;

	pos1 = pkt->find(0, search_gamename, strlen(search_gamename));
	pos1 += strlen(search_gamename) +1;

	/* hostport angabe suchen */
	pos2 = pkt->find(0, search_hostport, strlen(search_hostport));
	if (pos2 != -1) {
		pos2 += strlen(search_hostport) +1;
		pkt->parse_int(pos2, &port);
	}

	switch (gameid) {
	case ID_HALO:
	case ID_PK:
		break;

	case ID_SWAT4:
		if (pos2 == -1)
			return PARSE_REJECT;
		break;

	case ID_BF2:/* battlefield 2 */
		// TODO: pos2 check noetig?
		if (pos2 == -1) {
			LogSystem::log(LOG_DEBUG, "no hostport found");
			return PARSE_REJECT;
		}

		if (pkt->find(pos1, reply_bf2, strlen(reply_bf2)) == -1) {
			LogSystem::log(LOG_DEBUG, "no gamename found");
			return PARSE_REJECT;
		}
		break;

	default:
		return PARSE_REJECT;
		break;
	}

	/*
	 * wenn ein hostport angegeben wurde, und das nicht der src port ist
	 * beide ports in die serverliste uebernehmen
	 */
	if ((pos2 != -1) && (port != pkt->getPort())) {
		glist->addGame(gameid, pkt, port);

	} else {
		glist->addGame(gameid, pkt);
	}

	return PARSE_ACCEPT;
}
