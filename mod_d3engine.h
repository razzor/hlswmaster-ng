#ifndef _MODD3ENGINE_H_
#define _MODD3ENGINE_H_

#include "module.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"

class ModD3Engine : public Module {
public:
	ModD3Engine() {}
	~ModD3Engine() {}

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* slist);

	const char* getName() { return "Doom3 protocol"; }
};

#endif // _MODD3ENGINE_H_
