#ifndef _TIMERSERVICE_H_
#define _TIMERSERVICE_H_

#include "mutex.h"

class Event {
public:
	virtual ~Event() {};
	virtual void execute() =0;

protected:
	Event() {};
};

class Timer {
friend class TimerService;
public:
	Timer(Event* event, unsigned int timeval);
	~Timer();

protected:
	Timer(const Timer& te);
	Timer& operator=(const Timer& te);

private:
	Timer* next;
	Event* event;
	int timeval;
	long timeout;
};

class TimerService {
public:
	static void registerTimer(Timer* te);
	static void checkTimers();

protected:
	TimerService(const TimerService& ts);
	TimerService& operator=(const TimerService& ts);

private:
	TimerService() {}
	~TimerService();

	static TimerService* getInstance();

	void addTimer(Timer* te, bool trigger);
	void checkTimeouts();

	Timer* firstTimer;
	Mutex mutex;
};

#endif // _TIMERSERVICE_H_
