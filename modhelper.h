#ifndef _MODHELPER_H_
#define _MODHELPER_H_

#include "multisock.h"
#include "netpkt.h"

struct game_ports {
	int portlo;
	int porthi;
	int gameid;
};

class ModHelper {
public:
	static void send(MultiSock* msock, int port, const char* data, int size);
	static void send(MultiSock* msock, struct game_ports* arr, const char* data, int size);

	static int checkPorts(NetPkt* pkt, struct game_ports* arr);

protected:
	ModHelper() {};
	ModHelper(const ModHelper& m);
	ModHelper& operator=(const ModHelper& m);
};

#endif // _MODHELPER_H_
