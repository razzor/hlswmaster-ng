#ifndef _MULTISOCK_H_
#define _MULTISOCK_H_

#include <net/if.h>
#include <netinet/in.h>

#include "config.h"
#include "netpkt.h"
#include "socket.h"
#include "list.h"

class MultiSock {
public:
	MultiSock(Config& conf);
	~MultiSock();

	int sendto(NetPkt* pkt, struct sockaddr_in* dst = NULL);
	NetPkt* recv();

protected:
	MultiSock(const MultiSock& x);
	MultiSock& operator=(const MultiSock& x);

private:
	List<Socket> ifaceList;
	fd_set fdsel;
};

#endif // _MULTISOCK_H_
