/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include "logging.h"
#include "config.h"
#include "modulelist.h"
#include "gamelist.h"
#include "gamescanner.h"
#include "gameparser.h"
#include "hlswserver.h"
#include "timerservice.h"

#include "mod_d3engine.h"
#include "mod_gamespy1.h"
#include "mod_gamespy2.h"
#include "mod_halflife.h"
#include "mod_q3engine.h"
#include "mod_quake2.h"
#include "mod_ut.h"

#define DEFAULT_CONFIG "hlswmaster.conf"
#define DEFAULT_LOGFILE "hlswmaster.log"
#define DEFAULT_LOGPRIO "WARN"

static struct option opts[] = {
	{"config",	1, 0, 'c'},
	{"debug",	0, 0, 'd'},
	{"help",	0, 0, 'h'},
	{0, 0, 0, 0}
};

int main(int argc, char *argv[])
{
	LogSystem::init(DEFAULT_LOGPRIO, new StdErrLog());

	int arg = 0, code = 0, debug = 0;
	char *configfile = DEFAULT_CONFIG;
	while (code != -1) {
		code = getopt_long(argc, argv, "c:dh", opts, &arg);

		switch (code) {
		case 'c':	/* config */
				configfile = optarg;
				break;

		case 'd':	/* debug */
				debug = 1;
				break;

		case 'h':	/* help */
				printf("Usage: hlsw-master [options]\n"
					"Options: \n"
					"  --config       -c  configfile  use this configfile\n"
					"  --debug        -d              do not fork and log to stderr\n"
					"  --help         -h              this help\n"
					"\n");
				exit(0);
				break;

		case '?':	/* error */
				exit(-1);
				break;

		default:	/* unknown / all options parsed */
				break;
		}
	}

	Config conf;
	conf.parseFile(configfile);

	const char* logfile = conf.getString("global", "logfile", DEFAULT_LOGFILE);
	const char* logprio = conf.getString("global", "logprio", DEFAULT_LOGPRIO);
	if (!debug) {
		LogSystem::init(logprio, new FileLog(logfile));
		daemon(0, 0);
	} else {
		LogSystem::init(logprio, new StdErrLog());
	}

	LogSystem::log(LOG_EVERYTIME, "hlswmaster-ng %s startup (pid:%d)", VERSION, getpid());
	conf.show();

	ModuleList modList(conf);
	GameList gameList(conf);

	GameScanner scanner(conf, modList);
	GameParser parser(scanner, modList, gameList);
	HlswServer server(conf, gameList);

	modList.reg(new ModD3Engine());
	modList.reg(new ModGameSpy1());
	modList.reg(new ModGameSpy2());
	modList.reg(new ModHalfLife());
	modList.reg(new ModQ3Engine());
	modList.reg(new ModQuake2());
	modList.reg(new ModUT());

	server.start();
	parser.start();
	scanner.start();

	LogSystem::log(LOG_EVERYTIME, "hlswmaster-ng running...");

	while (1) {
		TimerService::checkTimers();
		usleep(500000);

		if (!scanner.isRunning()) {
			LogSystem::log(LOG_CRIT, "Scanner aborted!");
			break;
		}

		if (!parser.isRunning()) {
			LogSystem::log(LOG_CRIT, "RecvParser aborted!");
			break;
		}

		if (!server.isRunning()) {
			LogSystem::log(LOG_CRIT, "HlswServer aborted!");
			break;
		}
	}

	return -1;
}
