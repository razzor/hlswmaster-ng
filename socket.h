#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <net/if.h>
#include <netinet/in.h>

#include "netpkt.h"

class Socket : private ListEntry<Socket> {
public:
	Socket();
	~Socket();

	static Socket* createSocket(struct sockaddr_in* addr);
	static Socket* createSocket(const char* name, int port);

	int show(char* buf, int size);

	int sendto(NetPkt* pkt, struct sockaddr_in* dst = NULL);
	NetPkt* recv();

	int getFD();

protected:
	Socket(const Socket& s);
	Socket& operator=(const Socket& s);

private:
	bool checkDeviceFlags(const char* name);
	bool bindToDevice(const char* name);
	bool bindToAddress(struct sockaddr_in* addr);
	bool setBroadcast(int flag);

	int getRecvSize();

	int fd;
	char devname[IFNAMSIZ];
	struct sockaddr_in addr;
};

#endif // _SOCKET_H_
