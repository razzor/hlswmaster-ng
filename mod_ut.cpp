/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "mod_ut.h"
#include "modhelper.h"

static struct game_ports port_arr[] = {
	{  7777,  7788, 0 },
	{ 10777, 10777, 0 },
	{ 0,0,0 }
};

static const char scanmsg_ut2k3[] = { 0x79, 0x00, 0x00, 0x00, 0x00 };
static const char scanmsg_ut2k4[] = { 0x80, 0x00, 0x00, 0x00, 0x00 };

void ModUT::scan(MultiSock* msock)
{
	ModHelper::send(msock, port_arr, scanmsg_ut2k3, sizeof(scanmsg_ut2k3));
	ModHelper::send(msock, port_arr, scanmsg_ut2k4, sizeof(scanmsg_ut2k4));
}

int ModUT::parse(NetPkt* pkt, GameList* glist)
{
/*
	int gameid;
	gameid = ModHelper::checkPorts(pkt, port_arr);
	if (!gameid)
		return PARSE_REJECT;
*/
	if (pkt->compare(0, scanmsg_ut2k3, sizeof(scanmsg_ut2k3))) {
		glist->addGame(ID_UT2K3, pkt);
		return PARSE_ACCEPT;
	}

	if (pkt->compare(0, scanmsg_ut2k4, sizeof(scanmsg_ut2k4))) {
		glist->addGame(ID_UT2K4, pkt);
		return PARSE_ACCEPT;
	}

	return PARSE_REJECT;
}
