/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "modulelist.h"
#include "logging.h"

ModuleList::ModuleList(Config& conf)
: conf(conf)
{
}

ModuleList::~ModuleList()
{
	while (!mlist.isEmpty())
		delete mlist.get();
}

void ModuleList::reg(Module* mod)
{
	LogSystem::log(LOG_NOTICE, "Registering module '%s'", mod->getName());
	mod->init(&conf);
	mlist.add(mod);
}

void ModuleList::scan(MultiSock* msock)
{
	Iterator<Module> *it = mlist.createIterator();
	while (it->hasNext())
		it->next()->scan(msock);

	delete it;
}

int ModuleList::parse(NetPkt* pkt, GameList* slist)
{
	int retval = PARSE_REJECT;

	Iterator<Module> *it = mlist.createIterator();
	while (it->hasNext()) {
		retval = it->next()->parse(pkt, slist);
		if (retval != PARSE_REJECT)
			break;
	}

	delete it;
	return retval;
}
