#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "list.h"

class Config {
public:
	~Config();

	bool parseFile(const char* name);
	void show();

	const char* getParameter(const char* section, const char* name);
	const char* getString(const char* section, const char* name, char* def);
	int getInteger(const char* section, const char* name, int def);

	Iterator<char>* createIterator(const char* section, const char* name);

private:
	class Tupel : private ListEntry<Tupel> {
	public:
		Tupel(const char* name, const char* value);
		~Tupel();

		const char* name;
		const char* value;
	};

	class Section : private ListEntry<Section> {
	public:
		class SectionIterator : public Iterator<char> {
		public:
			SectionIterator(Section* section, const char* name);
			~SectionIterator();
			bool hasNext();
			char* next();
			void remove() {}
			void reset();

		private:
			Iterator<Tupel>* it;
			Tupel* nexttup;
			const char* name;
		};

		Section(const char* name);
		~Section();

		bool addTupel(const char* name, const char* option);
		const char* getTupelValue(const char* name);
		void show();

		const char* name;

		List<Tupel> tupelList;
	};

	Section* addSection(const char* name);
	Section* getSection(const char* name);

	List<Section> sectionList;
};

#endif // _CONFIG_H_
