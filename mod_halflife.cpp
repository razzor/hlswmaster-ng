/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "mod_halflife.h"
#include "modhelper.h"

static struct game_ports port_arr[] = {
	{ 27015, 27024, ID_HL },
	{ 0,0,0 }
};

static const char scanmsg1[] = "\xff\xff\xff\xff""details";
static const char scanmsg2[] = "\xff\xff\xff\xff\x54";
static const char scanmsg3[] = "\xff\xff\xff\xff""TSource Engine Query";

static const char reply1[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0x49, 0x07 }; // I.
static const char reply2[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0x6D, 0x00 }; // m

void ModHalfLife::scan(MultiSock* msock)
{
	ModHelper::send(msock, port_arr, scanmsg1, strlen(scanmsg1));
	ModHelper::send(msock, port_arr, scanmsg2, strlen(scanmsg2));
	ModHelper::send(msock, port_arr, scanmsg3, strlen(scanmsg3));
}

int ModHalfLife::parse(NetPkt* pkt, GameList* glist)
{
	struct in_addr tmp;
	int port, count, pos;

	if (ModHelper::checkPorts(pkt, port_arr) == 0)
		return PARSE_REJECT;

	// eat weird connection attempts
	if (pkt->getSize() <= 4)
		return PARSE_ACCEPT_QUIRK;

	/* Halflife2 answer */
	if (pkt->compare(0, reply1, sizeof(reply1))) {
		glist->addGame(ID_HL2, pkt);
		return PARSE_ACCEPT;
	}

	/* Halflife1 short answer */
	if (pkt->compare(0, reply2, sizeof(reply2))) {
		glist->addGame(ID_HL, pkt);
		return PARSE_ACCEPT;
	}

	/* Halflife1 answer (with IP:port) */
	if (!pkt->compare(0, reply2, sizeof(reply2) -1))
		return PARSE_REJECT;

	/* try to parse server IP */
	pos = 5;
	if ((count = pkt->parse_ip(pos, &tmp)) < 7)
		return PARSE_REJECT;

	/* parse server port */
	pos += count +1;
	if ((count = pkt->parse_int(pos, &port)) == 0)
		return PARSE_REJECT;

	// IP is 0.0.0.0 or 127.0.0.1 -> do not use IP info
	if (tmp.s_addr == 0x00000000 || tmp.s_addr == 0x0100007F) {
		glist->addGame(ID_HL, pkt);

	} else {
		glist->addGame(ID_HL, &tmp, port);
	}

	return PARSE_ACCEPT;
}
