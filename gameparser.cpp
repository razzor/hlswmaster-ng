/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gameparser.h"
#include "logging.h"
#include "netpkt.h"

GameParser::GameParser(GameScanner& scanner, ModuleList& modList, GameList& gameList)
: scanner(scanner), modList(modList), gameList(gameList)
{
}

int GameParser::execute(void* arg)
{
	NetPkt* pkt;
	char buf[64], *p;
	int ret;

	while (1) {
		pkt = scanner.getPkt();
		ret = modList.parse(pkt, &gameList);

		switch (ret) {
		case PARSE_ACCEPT_FAKE:
			pkt->show(buf, sizeof(buf));
			LogSystem::log(LOG_NOTICE, "not supported Game: %s", buf);

		case PARSE_ACCEPT:
			delete pkt;

		case PARSE_ACCEPT_FREED:
			break;

		default:
		case PARSE_REJECT:
			pkt->show(buf, sizeof(buf));
			LogSystem::log(LOG_NOTICE, "unknown Packet: %s", buf);

			p = pkt->showfull();
			LogSystem::log(LOG_DEBUG, "%s", p);
			delete [] p;
			delete pkt;
			break;
		}
	}
	return 0;
}
