/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "mod_q3engine.h"
#include "modhelper.h"

static struct game_ports port_arr[] = {
	{ 27960, 27969, ID_Q3A }, // q3(6), ef(7), et25), rtcw(8)
	{ 28070, 28070, ID_JK2 }, // jk2(12)
	{ 28960, 28963, ID_COD }, // cod(31), cod:uo(42), cod2(48)
	{ 29070, 29070, ID_JK3 }, // jk3(27)
	{ 0,0,0 }
};

static const char scanmsg[] = "\xff\xff\xff\xffgetStatus";
static const char replyhead[] = "\xff\xff\xff\xffstatusResponse";

static const char search_version[] = "\\version\\";
static const char search_gamename[] = "\\gamename\\";

static const char reply_q3[] = "Q3 ";
static const char reply_ef[] = "ST:V HM ";
static const char reply_rtcw[] = "Wolf ";
static const char reply_et[] = "ET";
static const char reply_jk2[] = "JK2MP";
static const char reply_jk3[] = "JAmp";

static const char reply_cod[] = "Call of Duty\\";
static const char reply_coduo[] = "CoD:United Offensive\\";
static const char reply_cod2[] = "Call of Duty 2\\";

void ModQ3Engine::scan(MultiSock* msock)
{
	ModHelper::send(msock, port_arr, scanmsg, strlen(scanmsg));
}

int ModQ3Engine::parse(NetPkt* pkt, GameList* glist)
{
	int gameid = 0, pos1, pos2;

	gameid = ModHelper::checkPorts(pkt, port_arr);
	if (!gameid)
		return PARSE_REJECT;

	if (!pkt->compare(0, replyhead, strlen(replyhead)))
		return PARSE_REJECT;

	pos1 = pkt->find(0, search_version, strlen(search_version));
	if (pos1 != -1) {
		pos1 += strlen(search_version);
		if (pkt->compare(pos1, reply_q3, strlen(reply_q3)))
			gameid = ID_Q3A;

		else if (pkt->compare(pos1, reply_ef, strlen(reply_ef)))
			gameid = ID_EF;

		else if (pkt->compare(pos1, reply_rtcw, strlen(reply_rtcw)))
			gameid = ID_RTCW;

		else if (pkt->compare(pos1, reply_et, strlen(reply_et)))
			gameid = ID_ET;

		else if (pkt->compare(pos1, reply_jk2, strlen(reply_jk2)))
			gameid = ID_JK2;

		else if (pkt->compare(pos1, reply_jk3, strlen(reply_jk3)))
			gameid = ID_JK3;
	}

	pos2 = pkt->find(0, search_gamename, strlen(search_gamename));
	if (gameid == 0 && pos2 != -1) {
		pos2 += strlen(search_gamename);
		if (pkt->compare(pos2, reply_cod, strlen(reply_cod)))
			gameid = ID_COD;

		else if (pkt->compare(pos2, reply_coduo, strlen(reply_coduo)))
			gameid = ID_COD_UO;

		else if (pkt->compare(pos2, reply_cod2, strlen(reply_cod2)))
			gameid = ID_COD2;
	}

	if (gameid == 0)
		return PARSE_REJECT;

	glist->addGame(gameid, pkt);
	return PARSE_ACCEPT;
}
