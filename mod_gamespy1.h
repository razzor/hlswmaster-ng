#ifndef _MODGAMESPY1_H_
#define _MODGAMESPY1_H_

#include "module.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"
#include "list.h"
#include "timerservice.h"

class ModGameSpy1 : public Module {
public:
	ModGameSpy1();
	~ModGameSpy1();

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* glist);

	const char* getName() { return "GameSpy 1 protocol"; }

	void gc();

private:
	class MultiPart : private ListEntry<MultiPart> {
	public:
		MultiPart(NetPkt* pkt, int queryid, int subid);
		~MultiPart();

		NetPkt* pkt;
		int queryid;
		int subid;

		long timeout;
	};

	class GcEvent : public Event {
	public:
		GcEvent(ModGameSpy1& mgs1) : mgs1(mgs1) {}
		~GcEvent() {}
		void execute() { mgs1.gc(); }

	private:
		ModGameSpy1& mgs1;
	};

	NetPkt* merge(NetPkt* pkt, int queryid, int subid);
	int parse_real(NetPkt* pkt, GameList* glist, int gameid);

	LockedList<MultiPart> list;
};

#endif // _MODGAMESPY1_H_
