#ifndef _MODULELIST_H_
#define _MODULELIST_H_

#include "config.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"
#include "module.h"
#include "list.h"

class ModuleList {
public:
	ModuleList(Config& conf);
	~ModuleList();

	void reg(Module* mod);

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* slist);

protected:
	ModuleList(const ModuleList& ml);
	ModuleList& operator=(const ModuleList& ml);

private:
	List<Module> mlist;
	Config& conf;
};

#endif // _MODULELIST_H_
