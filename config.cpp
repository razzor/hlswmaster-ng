/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include "logging.h"

#define BUFSIZE 1024

/* ------ */

Config::Tupel::Tupel(const char* name_, const char* value_)
{
	name = strdup(name_);
	value = strdup(value_);
}

Config::Tupel::~Tupel()
{
	free((char*)name);
	free((char*)value);
}

/* ------ */

Config::Section::Section(const char* name_)
{
	name = strdup(name_);
}

Config::Section::~Section()
{
	while (!tupelList.isEmpty())
		delete tupelList.get();

	free((char*)name);
}

bool Config::Section::addTupel(const char* name, const char* value)
{
	if (!name || !value)
		return false;

	Tupel* t = new Tupel(name, value);
	tupelList.add(t);

	return true;
}

void Config::Section::show()
{
	Iterator<Tupel>* it = tupelList.createIterator();
	LogSystem::log(LOG_INFO, "[%s]", name);

	while (it->hasNext()) {
		Tupel* t = it->next();
		LogSystem::log(LOG_INFO, " %s = %s", t->name, t->value);
	}

	delete it;
}

const char* Config::Section::getTupelValue(const char* name)
{
	const char* retval = 0;
	Iterator<Tupel>* it = this->tupelList.createIterator();
	while (it->hasNext()) {
		Tupel* t = it->next();
		if (!strcmp(t->name, name)) {
			retval = t->value;
			break;
		}
	}

	delete it;
	return retval;
}

/* ------ */

Config::Section::SectionIterator::SectionIterator(Section* section, const char* name)
: nexttup(0), name(name)
{
	it = section->tupelList.createIterator();
}

Config::Section::SectionIterator::~SectionIterator()
{
	delete it;
}

bool Config::Section::SectionIterator::hasNext()
{
	while (it->hasNext()) {
		nexttup = it->next();
		if (!strcmp(nexttup->name, name))
			return true;
	}
	return false;
}

char* Config::Section::SectionIterator::next()
{
	return (char*)nexttup->value;
}

void Config::Section::SectionIterator::reset()
{
	it->reset();
	nexttup = 0;
}

/* ------ */

Config::~Config()
{
	while (!sectionList.isEmpty())
		delete sectionList.get();
}

Config::Section* Config::addSection(const char* name)
{
	Section* s = new Section(name);
	sectionList.add(s);
	return s;
}

bool Config::parseFile(const char* name)
{
	Section* section = 0;
	FILE *fz;
	int i = 0;
	char *row, *tok, *tok2;
	bool ret = true;

	row = new char[BUFSIZE];
	if (!row) {
		LogSystem::log(LOG_ERROR, "config_parse(): out of memory()");
		return false;
	}

	if (!(fz = fopen(name, "r"))) {
		LogSystem::log(LOG_ERROR, "config_parse(): can not open %s", name);
		delete [] row;
		return false;
	}

	while (fgets(row, BUFSIZE, fz)) {
		i++;

		/* kommentar oder leere zeile */
		if (row[0] == '#' || row[0] <= ' ') {
			continue;

		/* neue section */
		} else if (row[0] == '[') {
			tok = strtok(row +1, " ]\n");
			section = addSection(tok);
			if (!section) {
				LogSystem::log(LOG_WARN, "config_parse(): invalid section in row %d", i);
				ret = false;
				break;
			}
			continue;

		/* option, aber es gab noch keine section */
		} else if (!section) {
			LogSystem::log(LOG_WARN, "config_parse(): missing section in row %d", i);
			ret = false;
			break;
		}

		/* option */
		if ((tok = strtok(row, " \n"))) {
			while ((tok2 = strtok(NULL, " \n"))) {
				if (!section->addTupel(tok, tok2))
					LogSystem::log(LOG_WARN, "config_parse(): invalid row %d", i);
			}
		}
	}

	fclose(fz);
	delete [] row;

	return ret;
}

void Config::show()
{
	LogSystem::log(LOG_INFO, "Config Dump:");
	Iterator<Section>* it = sectionList.createIterator();
	while (it->hasNext())
		it->next()->show();

	delete it;
}

Config::Section* Config::getSection(const char* name)
{
	Section* retval = 0;

	Iterator<Section>* it = sectionList.createIterator();
	while (it->hasNext()) {
		Section* s = it->next();
		if (!strcmp(s->name, name)) {
			retval = s;
			break;
		}
	}

	delete it;
	return retval;
}

const char* Config::getParameter(const char* section, const char* option)
{
	Section* s = getSection(section);
	return s ? s->getTupelValue(option) : 0;
}

const char* Config::getString(const char* section, const char* option, char* def)
{
	const char* retval = getParameter(section, option);
	if (!retval) {
		LogSystem::log(LOG_NOTICE,
			"Config: [%s:%s] not found => using '%s'",
			section, option, def);
		return def;
	}
	return retval;
}

int Config::getInteger(const char* section, const char* option, int def)
{
	const char* retval = getParameter(section, option);
	if (!retval) {
		LogSystem::log(LOG_NOTICE,
			"Config: [%s:%s] not found => using '%d'",
			section, option, def);
		return def;
	}
	return atoi(retval);
}

Iterator<char>* Config::createIterator(const char* section, const char* name)
{
	Section* s = getSection(section);
	if (!s) {
		LogSystem::log(LOG_NOTICE,"Config: [%s:%s] not found",
			section, name);

		return new NullIterator<char>();

	} else {
		return new Config::Section::SectionIterator(s, name);
	}
}
