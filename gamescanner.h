#ifndef _GAMESCANNER_H_
#define _GAMESCANNER_H_

#include "thread.h"
#include "config.h"
#include "modulelist.h"
#include "timerservice.h"
#include "multisock.h"
#include "semaphore.h"
#include "list.h"
#include "netpkt.h"

class GameScanner : public Thread {
public:
	GameScanner(Config& conf, ModuleList& modList);
	~GameScanner();

	int execute(void* arg);
	void scan();

	NetPkt* getPkt();

protected:
	GameScanner(const GameScanner& hs);
	GameScanner& operator=(const GameScanner& hs);

private:
	class ScanEvent : public Event {
	public:
		ScanEvent(GameScanner& gs) : gs(gs) {}
		~ScanEvent() {}
		void execute() { gs.scan(); }

	private:
		GameScanner& gs;
	};

	ModuleList& modList;
	MultiSock* msock;

	Semaphore pktCount;
	LockedList<NetPkt> pktList;
};

#endif // _SCANNER_H_
