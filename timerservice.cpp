/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <time.h>

#include "timerservice.h"

// TODO: how to deregister TimerEvents?

Timer::Timer(Event* event, unsigned int timeval)
: next(0), event(event), timeval(timeval)
{
}

Timer::~Timer()
{
	delete event;
}

TimerService::~TimerService()
{
	Timer* search = firstTimer;
	while (search != NULL) {
		Timer* next = search->next;
		delete search;
		search = next;
	}
}

void TimerService::addTimer(Timer* te, bool trigger)
{
	te->timeout = time(NULL) + ((!trigger) ? te->timeval : 0);

	Timer** search = &firstTimer;
	while ((*search) != NULL && (*search)->timeout <= te->timeout)
		search = &((*search)->next);

	te->next = (*search);
	(*search) = te;
}

void TimerService::checkTimeouts()
{
	long now = time(NULL);
	Timer* search = firstTimer;
	while (search != NULL && search->timeout <= now) {
		search->event->execute();

		firstTimer = search->next;
		addTimer(search, false);

		search = firstTimer;
	}
}

TimerService* TimerService::getInstance()
{
	static TimerService ts;
	return &ts;
}

void TimerService::registerTimer(Timer* te)
{
	TimerService* ts = getInstance();

	AutoMutex am(ts->mutex);
	ts->addTimer(te, true);
}

void TimerService::checkTimers()
{
	TimerService* ts = getInstance();

	AutoMutex am(ts->mutex);
	ts->checkTimeouts();
}
