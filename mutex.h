#ifndef _MUTEX_H_
#define _MUTEX_H_

#include <pthread.h>

class Mutex {
public:
	Mutex()		{ pthread_mutex_init(&m, 0); }
	~Mutex()	{ pthread_mutex_destroy(&m); }
	void lock()	{ pthread_mutex_lock(&m); }
	void unlock()	{ pthread_mutex_unlock(&m); }

protected:
	Mutex(const Mutex& m);
	Mutex& operator=(const Mutex& m);

private:
	pthread_mutex_t m;
};


class AutoMutex {
public:
	AutoMutex(Mutex& x) : m(x)	{ m.lock(); }
	~AutoMutex()			{ m.unlock(); }

protected:
	AutoMutex(const AutoMutex& am);
	AutoMutex& operator=(const AutoMutex& am);

private:
	Mutex& m;
};

#endif //_MUTEX_H_
