/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "mod_quake2.h"
#include "modhelper.h"

#define QUAKE2_PORT	27910

// scan for latest protocol version
static const char scanmsg[] = "\xff\xff\xff\xffinfo 34";
static const char replyhead[] = "\xff\xff\xff\xffinfo";

void ModQuake2::scan(MultiSock* msock)
{
	ModHelper::send(msock, QUAKE2_PORT, scanmsg, strlen(scanmsg));
}

int ModQuake2::parse(NetPkt* pkt, GameList* glist)
{
	if (pkt->getPort() != QUAKE2_PORT)
		return PARSE_REJECT;

	if (!pkt->compare(0, replyhead, strlen(replyhead)))
		return PARSE_REJECT;

	glist->addGame(ID_Q2, pkt);
	return PARSE_ACCEPT;
}
