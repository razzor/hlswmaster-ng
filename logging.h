#ifndef _LOGGING_H_
#define _LOGGING_H_

#include <stdio.h>

// really noisy debugging (pkt-dumps)
#define LOG_DEBUG 5

// normal "debug" (config, recv. packets)
#define LOG_INFO  4

// interesting stuff (unknown packets)
#define LOG_NOTICE 3

// something is not right, but programm is still working (config errors)
#define LOG_WARN 2

// something is *really* bad, but we try to keep up (mem-allocs)
#define LOG_ERROR 1

// we must bailout *now*
#define LOG_CRIT 0

#define LOG_EVERYTIME 0

class LogOutput {
public:
	virtual ~LogOutput() {};
	virtual void write(const char* buf) {};
};

class LogSystem {
public:
	static void init(int prio, LogOutput* lo);
	static void init(const char* prio, LogOutput* lo);
	static void log(int prio, const char* fmt, ...);

private:
	LogSystem();
	~LogSystem();
	static LogSystem* getInstance();

	LogOutput *output;
	int priority;
	char buffer[8192];	// we need this for (large) packet dumps!
};

class StdErrLog : public LogOutput {
public:
	void write(const char* buf);
};

class FileLog : public LogOutput {
public:
	FileLog(const char* filename);
	~FileLog();

	void write(const char* buf);

private:
	FILE* logfile;
};

#endif // _LOGGING_H_
