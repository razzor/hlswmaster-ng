/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "modhelper.h"

void ModHelper::send(MultiSock* msock, int port, const char* data, int size)
{
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = 0xFFFFFFFF;
	addr.sin_port = htons(port);

	NetPkt* pkt = new NetPkt(data, size);
	pkt->setAddress(&addr);
	msock->sendto(pkt);

	delete pkt;
}

void ModHelper::send(MultiSock* msock, struct game_ports* arr, const char* data, int size)
{
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = 0xFFFFFFFF;

	NetPkt* pkt = new NetPkt(data, size);

	while (arr && arr->portlo && arr->porthi) {
		int port;
		for (port = arr->portlo; port <= arr->porthi; port++) {
			addr.sin_port = htons(port);
			pkt->setAddress(&addr);
			msock->sendto(pkt);
		}
		arr++;
	}

	delete pkt;
}

int ModHelper::checkPorts(NetPkt* pkt, struct game_ports* arr)
{
	int myport = ntohs(pkt->getAddress()->sin_port);
	while (arr && arr->portlo) {
		int port;
		for (port = arr->portlo; port <= arr->porthi; port++)
			if (port == myport)
				return arr->gameid;

		arr++;
	}
	return 0;
}
