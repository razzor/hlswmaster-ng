#ifndef _THREAD_H_
#define _THREAD_H_

#include <pthread.h>

class Thread {
public:
	Thread();
	virtual ~Thread();

	virtual int execute(void* arg) =0;

	int start(void* arg = 0);
	void cancel();
	int join();
	bool isRunning() const;

protected:
	Thread(const Thread& t);
	Thread& operator=(const Thread& t);

private:
	void* arg;
	pthread_t tid;
	volatile int state;

	static void* entry(void* myself);
};

#endif // _THREAD_H_
