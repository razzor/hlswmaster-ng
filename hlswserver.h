#ifndef _HLSWSERVER_H_
#define _HLSWSERVER_H_

#include <string.h>

#include "thread.h"
#include "config.h"
#include "gamelist.h"
#include "timerservice.h"
#include "netpkt.h"
#include "socket.h"
#include "mutex.h"

class HlswServer : public Thread {
public:
	HlswServer(Config& conf, GameList& glist);
	~HlswServer();

	int execute(void* arg);
	void rebuild(GameList& gl);

protected:
	HlswServer(const HlswServer& hs);
	HlswServer& operator=(const HlswServer& hs);

private:
	class RebuildEvent : public Event {
	public:
		RebuildEvent(HlswServer& hs, GameList& gl) : hs(hs), gl(gl) {}
		~RebuildEvent() {}
		void execute() { hs.rebuild(gl); }

	private:
		HlswServer& hs;
		GameList& gl;
	};

	void addGame(NetPkt* pkt, GameEntry* entry);

	List<NetPkt>* pktlist;
	Mutex mutex;
	Socket* socket;
};

#endif // _HLSWSERVER_H_
