#ifndef _MODULE_H_
#define _MODULE_H_

#include "list.h"
#include "config.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"

enum {
	ID_UNKNOWN	= 0,	// "Unknown"
	ID_HL,			// "Halflife"
	ID_Q1,			// "Quake 1"
	ID_Q2,			// "Quake 2"
	ID_Q3COMP,		// "Q3Comp"
	ID_UT		= 5,	// "Unreal Tournament"
	ID_Q3A,			// "Quake 3 Arena"
	ID_EF,			// "Elite Force"
	ID_RTCW,		// "Return to Castle Wolfenstein"
	ID_GS1PROT,		// "GSProt"
	ID_CCR		= 10,	// "Command & Conquer Renegade"
	ID_MOHAA,		// "Medal of Honor: Allied Assault"
	ID_JK2,			// "Jedi Knight 2"
	ID_SOF,			// "Soldier of Fortune"
	ID_UT2K3,		// "Unreal Tournament 2003"
	ID_AAO		= 15,	// "America's Army: Operations"
	ID_BF1942,		// "Battlefield 1942"
	ID_AVP2,		// "Alien vs. Predator 2"
	ID_RUNE,		// "Rune"
	ID_IGI2,		// "Project IGI2: Covert Strike"
	ID_NWN		= 20,	// "Never Winter Nights"
	ID_MOHAA_S,		// "Medal of Honor: Allied Assault Spearhead"
	ID_OPFP,		// "Operation Flashpoint"
	ID_OPFPR,		// "Operation Flashpoint Resistance"
	ID_DEVA,		// "Devastation"
	ID_ET		= 25,	// "Wolfenstein - Enemy Territory"
	ID_EF2,			// "Elite Force 2"
	ID_JK3,			// "Jedi Knight 3"
	ID_MOHAA_B,		// "Medal of Honor: Allied Assault Breakthrough"
	ID_TRIBES2,		// "Tribes 2"
	ID_HALO		= 30,	// "Halo"
	ID_COD,			// "Call of Duty"
	ID_SAVAGE,		// "Savage: The Battle for Newerth"
	ID_UT2K4,		// "Unreal Tournament 2004"
	ID_HLSTEAM,		// "HLSteam"
	ID_BFV		= 35,	// "Battlefield Vietnam"
	ID_GS2PROT,		// "GS2Prot"
	ID_PK,			// "Pain Killer"
	ID_D3,			// "Doom 3"
	ID_OGPPROT,		// "OGPProt"
	ID_HL2		= 40,	// "Halflife 2"
	ID_TRIBES_V,		// "Tribes Vengeance"
	ID_COD_UO,		// "Call of Duty: United Offensive"
	ID_SW_BF,		// "Starwars: Battlefront (?)"
	ID_SWAT4,		// "SWAT 4"
	ID_BF2		= 45,	// "Battlefield 2"
	ID_xxx,			// "???"
	ID_Q4,			// "Quake 4"
	ID_COD2			// "Call of Duty 2"
};

class Module : private ListEntry<Module> {
public:
	virtual ~Module() {};
	virtual void init(Config* conf) {}
	virtual void scan(MultiSock* msock) =0;
	virtual int parse(NetPkt* pkt, GameList* glist) =0;
	virtual const char* getName() =0;

protected:
	Module() {};
	Module(const Module& m);
	Module& operator=(const Module& m);
};

#endif // _MODULE_H_
