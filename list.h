#ifndef _LIST_H_
#define _LIST_H_

#include "mutex.h"

template <class T>
class Iterator {
public:
	virtual ~Iterator()	{}
	virtual bool hasNext()	=0;
	virtual T* next()	=0;
	virtual void remove()	=0;
	virtual void reset()	=0;

protected:
	Iterator()		{}
	Iterator(const Iterator& it);
	Iterator& operator=(const Iterator& it);
};


template <class T>
class NullIterator : public Iterator<T> {
public:
	NullIterator()		{}
	virtual ~NullIterator()	{}
	virtual bool hasNext()	{ return false; }
	virtual T* next()	{ return NULL; }
	virtual void remove()	{}
	virtual void reset()	{}
};


template <class T>
class ListEntry;


template <class T>
class ListIterator;


template <class T>
class List {
friend class ListIterator<T>;
public:
	List() : head(0), tail(0) {}
	~List() {}

	void add(T* entry)
	{
		ListEntry<T>* tmp = (ListEntry<T>*)entry;
		tmp->next = NULL;

		if (!head)
			head = tmp;

		if (tail)
			tail->next = tmp;
		tail = tmp;
	}

	T* get()
	{
		ListEntry<T>* retval = head;

		if (head)
			head = head->next;

		if (!head)
			tail = NULL;

		return (T*)retval;
	}

	T* peekHead()
	{
		return (T*)head;
	}

	T* peekTail()
	{
		return (T*)tail;
	}

	bool isEmpty()
	{
		return (head == NULL);
	}

	Iterator<T>* createIterator()
	{
		return new ListIterator<T>(this);
	}

protected:
	List(const List& l);
	List& operator=(const List& l);

private:
	ListEntry<T> *head;
	ListEntry<T> *tail;
};


template <class T>
class ListIterator : public Iterator<T> {
public:
	ListIterator(List<T>* list)
	: list(list)
	{
		reset();
	}

	virtual ~ListIterator() {}

	virtual bool hasNext()
	{
		return (pos == NULL) ? (list->head != NULL) : (pos->next != NULL);
	}

	virtual T* next()
	{
		prev = pos;
		pos = (pos == NULL) ? list->head : pos->next;
		return (T*)pos;
	}

	virtual void remove()
	{
		// remove pre-head -> bail out
		if (pos == NULL)
			return;

		// remove first
		if (pos == list->head)
			list->head = pos->next;

		// remove middle
		if (prev)
			prev->next = pos->next;

		// remove last
		if (pos == list->tail)
			list->tail = (prev == NULL) ? NULL : prev;

		pos = prev;
	}

	virtual void reset()
	{
		pos = NULL;
		prev = NULL;
	}

protected:
	ListIterator(const ListIterator& li);
	ListIterator& operator=(const ListIterator& li);

	List<T>* list;

private:
	ListEntry<T>* pos;
	ListEntry<T>* prev;
};


template <class T>
class ListEntry {
friend class List<T>;
friend class ListIterator<T>;
public:
	ListEntry() {}
	~ListEntry() {}

private:
	ListEntry* next;
};


template <class T>
class LockedListIterator;


template <class T>
class LockedList : private List<T> {
friend class LockedListIterator<T>;
public:
	LockedList() {}
	~LockedList() {}
	void add(T* entry)
	{
		AutoMutex am(mutex);
		List<T>::add(entry);
	}

	T* get()
	{
		AutoMutex am(mutex);
		return List<T>::get();
	}

	T* peekFirst()
	{
		return NULL; // not with lockedlist
	}

	T* peekLast()
	{
		return NULL; // not with lockedlist
	}

	bool isEmpty()
	{
		AutoMutex am(mutex);
		return List<T>::isEmpty();
	}

	Iterator<T>* createIterator()
	{
		return new LockedListIterator<T>(this);
	}

private:
	Mutex mutex;
};


template <class T>
class LockedListIterator : public ListIterator<T> {
public:
	LockedListIterator(LockedList<T>* list)
	: ListIterator<T>(list)
	{
		list->mutex.lock();
	}

	virtual ~LockedListIterator()
	{
		((LockedList<T>*)list)->mutex.unlock();
	}
};

#endif //_LIST_H_
