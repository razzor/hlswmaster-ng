#ifndef _MODQUAKE2_H_
#define _MODQUAKE2_H_

#include "module.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"

class ModQuake2 : public Module {
public:
	ModQuake2() {}
	~ModQuake2() {}

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* slist);

	const char* getName() { return "Quake2 protocol"; }
};

#endif // _MODQUAKE2_H_
