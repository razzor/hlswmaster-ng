#ifndef _GAMELIST_H_
#define _GAMELIST_H_

#include <netinet/in.h>

#include "config.h"
#include "netpkt.h"
#include "gameentry.h"
#include "timerservice.h"
#include "list.h"
#include "mutex.h"

// 127 is prime!
#define MAX_BUCKETS 127

class GameList {
public:
	GameList(Config& conf);
	~GameList();

	void cleanup();
	void addGame(int gameid, NetPkt* pkt, int port2 = 0);
	void addGame(int gameid, struct in_addr *addr, int port1, int port2 = 0);
	Iterator<GameEntry>* createIterator();

protected:
	GameList(const GameList& gl);
	GameList& operator=(const GameList& gl);

private:
	class CleanupEvent : public Event {
	public:
		CleanupEvent(GameList& gl) : gl(gl) {}
		~CleanupEvent() {}
		void execute() { gl.cleanup(); }

	private:
		GameList& gl;
	};

	class GameListIterator : public Iterator<GameEntry> {
	public:
		GameListIterator(GameList* gl);
		~GameListIterator();

		bool hasNext();
		GameEntry* next();
		void remove();
		void reset();

	private:
		GameList* gl;
		Iterator<GameEntry>* it;
		int bucket;
	};

	Mutex mutex;
	List<GameEntry> buckets[MAX_BUCKETS];

	int timeout;

friend class GameList::GameListIterator;
};

#endif // _GAMELIST_H_
