/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>

#include "logging.h"

LogSystem::LogSystem()
: output(0), priority(0)
{
}

LogSystem::~LogSystem()
{
	if (output)
		delete output;
}

LogSystem* LogSystem::getInstance()
{
	static LogSystem ls;
	return &ls;
}

void LogSystem::init(int prio, LogOutput* out)
{
	LogSystem* ls = getInstance();
	if (out) {
		if (ls->output)
			delete ls->output;

		ls->output = out;
	}
	ls->priority = prio;
}

void LogSystem::init(const char* prio, LogOutput* out)
{
	if (!strcasecmp(prio, "DEBUG"))
		init(LOG_DEBUG, out);

	else if (!strcasecmp(prio, "INFO"))
		init(LOG_INFO, out);

	else if (!strcasecmp(prio, "NOTICE"))
		init(LOG_NOTICE, out);

	else if (!strcasecmp(prio, "WARN"))
		init(LOG_WARN, out);

	else if (!strcasecmp(prio, "ERROR"))
		init(LOG_ERROR, out);

	else if (!strcasecmp(prio, "CRIT"))
		init(LOG_CRIT, out);

	else
		log(LOG_CRIT, "invalid logging priority");
}

void LogSystem::log(int prio, const char* fmt, ...)
{
	LogSystem* ls = getInstance();

	if (prio > ls->priority || !ls->output)
		return;

	va_list az;
	int len;

	va_start(az, fmt);
	len = vsnprintf(ls->buffer, sizeof(ls->buffer), fmt, az);
	va_end(az);

	if (errno) {
		strncpy(ls->buffer + len, ": ", sizeof(ls->buffer) - len);
		len += 2;
		strncpy(ls->buffer + len, strerror(errno), sizeof(ls->buffer) - len);

		errno = 0;
	}

	ls->output->write(ls->buffer);
}

/* ------- */

void StdErrLog::write(const char* buf)
{
	fprintf(stderr, "%s\n", buf);
}

/* ------- */

FileLog::FileLog(const char* filename)
{
	if (!(logfile = fopen(filename, "a" )))
		LogSystem::log(LOG_CRIT, "Can not open logfile '%s'", filename);

	fprintf(logfile, "-----------------\n");
}

FileLog::~FileLog()
{
	fclose(logfile);
}

void FileLog::write(const char* buf)
{
	time_t  tzgr;
	char    tbuf[64];
	time(&tzgr);
	strftime(tbuf, sizeof(tbuf), "%b %d %H:%M:%S :", localtime(&tzgr));

	fprintf(logfile, "%s %s\n", tbuf, buf);
	fflush(logfile);
}
