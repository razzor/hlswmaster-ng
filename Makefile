VERSION := v1.01

CFLAGS := -O2 -pipe -Wall -DVERSION='"$(VERSION)"'
CXXFLAGS := -O2 -pipe -Wall -Wunused -Wno-deprecated -DVERSION='"$(VERSION)"'
LIBS := -lpthread

HLSWMASTER_SRC := config.o gameentry.o gamelist.o gameparser.o gamescanner.o \
	hlswmaster.o hlswserver.o logging.o modhelper.o modulelist.o \
	multisock.o netpkt.o socket.o timerservice.o thread.o \
	mod_d3engine.o mod_gamespy1.o mod_gamespy2.o mod_halflife.o \
	mod_q3engine.o mod_quake2.o mod_ut.o

all: hlswmaster masterquery

hlswmaster: $(HLSWMASTER_SRC) hlswmaster.o
	$(CXX) $(CXXFLAGS) $^ $(LIBS) -o $@

masterquery: masterquery.o
	$(CC) $(CFLAGS) $^ $(LIBS) -o $@

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

%.d: %.cpp
	$(CXX) $(CXXFLAGS) -MM -c $< -o $@

dist: clean
	ln -s . hlswmaster-ng
	tar -czhf hlswmaster-ng-$(VERSION).tar.gz hlswmaster-ng/ \
	    --exclude hlswmaster-ng/.git \
	    --exclude hlswmaster-ng/.gitignore \
	    --exclude hlswmaster-ng/doc \
	    --exclude hlswmaster-ng/hlswmaster-ng-$(VERSION).tar.gz \
	    --exclude hlswmaster-ng/hlswmaster-ng
	rm hlswmaster-ng

clean:
	rm -f hlswmaster masterquery *.d *.o *.log \
	    hlswmaster-ng hlswmaster-ng-$(VERSION).tar.gz

##DEPS := $(wildcard *.cpp)
##-include $(DEPS:.cpp=.d)
