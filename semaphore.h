#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

#include <semaphore.h>

class Semaphore {
public:
	Semaphore()		{ sem_init(&s, 0, 0); }
	Semaphore(int i)	{ sem_init(&s, 0, i); }
	~Semaphore()		{ sem_destroy(&s); }
	void wait()		{ sem_wait(&s); }
	void post()		{ sem_post(&s); }

protected:
	Semaphore(const Semaphore& s);
	Semaphore& operator=(const Semaphore& s);

private:
	sem_t s;
};

#endif //_SEMAPHORE_H_
