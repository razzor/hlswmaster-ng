/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gameentry.h"
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>

#include "gameentry.h"

GameEntry::GameEntry(int gameid, struct in_addr *addr, int port1, int port2)
: port1(port1), port2(port2), gameid(gameid)
{
	memcpy(&this->addr, addr, sizeof(this->addr));

	if (port2 != 0) {
		this->port2 = this->port1;
		this->port1 = port2;
	}

	update();
}

int GameEntry::compare(const GameEntry* ge)
{
	if (this->addr.s_addr > ge->addr.s_addr)
		return -1;

	if (this->addr.s_addr < ge->addr.s_addr)
		return 1;

	if (this->port1 > ge->port1)
		return -1;

	if (this->port1 < ge->port1)
		return 1;

	// only compare IP:port
	// gameid and port2 are useless
	return 0;
}

int GameEntry::hash(int max)
{
	unsigned int hash = 0x12345678;

	// IP
	hash = ((hash<<5) ^ (hash>>27)) ^ ((addr.s_addr>>0) & 0xFF);
	hash = ((hash<<5) ^ (hash>>27)) ^ ((addr.s_addr>>8) & 0xFF);
	hash = ((hash<<5) ^ (hash>>27)) ^ ((addr.s_addr>>16) & 0xFF);
	hash = ((hash<<5) ^ (hash>>27)) ^ ((addr.s_addr>>24) & 0xFF);

	// port
	hash = ((hash<<5) ^ (hash>>27)) ^ ((port1>>0) & 0xFF);
	hash = ((hash<<5) ^ (hash>>27)) ^ ((port1>>8) & 0xFF);

	return hash % max;
}

void GameEntry::update()
{
	modtime = time(NULL);
}

int GameEntry::show(char* buf, int size)
{
	return snprintf(buf, size, "(%2d:%15s:%5d:%5d)",
		this->gameid, inet_ntoa(this->addr),
		this->port1, this->port2);
}
