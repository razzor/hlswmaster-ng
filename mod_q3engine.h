#ifndef _MODQ3ENGINE_H_
#define _MODQ3ENGINE_H_

#include "module.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"

class ModQ3Engine : public Module {
public:
	ModQ3Engine() {}
	~ModQ3Engine() {}

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* glist);

	const char* getName() { return "Quake3 protocol"; }
};

#endif // _MODQ3ENGINE_H_
