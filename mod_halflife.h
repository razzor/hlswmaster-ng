#ifndef _MODHALFLIFE_H_
#define _MODHALFLIFE_H_

#include "module.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"

class ModHalfLife : public Module {
public:
	ModHalfLife() {}
	~ModHalfLife() {}

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* glist);

	const char* getName() { return "Half-Life protocol"; }
};

#endif // _MODHALFLIFE_H_
