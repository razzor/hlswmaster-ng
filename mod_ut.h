#ifndef _MODUT_H_
#define _MODUT_H_

#include "module.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"

class ModUT : public Module {
public:
	ModUT() {}
	~ModUT() {}

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* slist);

	const char* getName() { return "UT protocol (native)"; }
};

#endif // _MODUT_H_
