#ifndef _GAMEPARSER_H_
#define _GAMEPARSER_H_

#include "thread.h"
#include "gamescanner.h"
#include "modulelist.h"
#include "gamelist.h"

class GameParser : public Thread {
public:
	GameParser(GameScanner& scanner, ModuleList& modlist, GameList& gameList);
	~GameParser() {};

	int execute(void* arg);

protected:
	GameParser(const GameParser& rp);
	GameParser& operator=(const GameParser& rp);

private:
	GameScanner& scanner;
	ModuleList& modList;
	GameList& gameList;
};

#endif // _GAMEPARSER_H_
