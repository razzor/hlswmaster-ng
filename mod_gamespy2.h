#ifndef _MODGAMESPY2_H_
#define _MODGAMESPY2_H_

#include "module.h"
#include "multisock.h"
#include "netpkt.h"
#include "gamelist.h"

class ModGameSpy2 : public Module {
public:
	ModGameSpy2() {}
	~ModGameSpy2() {}

	void scan(MultiSock* msock);
	int parse(NetPkt* pkt, GameList* glist);

	const char* getName() { return "GameSpy 2 protocol"; }
};

#endif // _MODGAMESPY1_H_
