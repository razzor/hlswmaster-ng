#ifndef _NETPKT_H_
#define _NETPKT_H_

#include <net/if.h>
#include <netinet/in.h>

#include "list.h"

#define PARSE_ACCEPT 1
#define PARSE_ACCEPT_FREED 2
#define PARSE_ACCEPT_FAKE 3
#define PARSE_ACCEPT_QUIRK PARSE_ACCEPT
#define PARSE_REJECT 4

/* avoid cyclic deps */
class Socket;

class NetPkt : private ListEntry<NetPkt> {
friend class Socket;
public:
	NetPkt(const char* data, int size);
	NetPkt(int alloc);
	~NetPkt();

	int show(char* buf, int size);
	char* showfull();

	bool compare(unsigned int offset, const char* buf, unsigned int len);
	int find(unsigned int offset, const char *buf, unsigned int len);

	int parse_int(unsigned int offset, int *val);
	int parse_ip(unsigned int offset, struct in_addr *ip);

	void setAddress(struct sockaddr_in *addr);
	struct sockaddr_in * getAddress();
	bool sameAddress(NetPkt* pkt);

	void merge(NetPkt* pkt);
	bool append(const char* buf, unsigned int len);

	int getPort();
	int getSize();

protected:
	NetPkt(const NetPkt& x);
	NetPkt& operator=(const NetPkt& x);

	struct sockaddr_in addr;
	const char *data;
	unsigned int size;

private:
	unsigned int alloc;
};

#endif // _NETPKT_H_
