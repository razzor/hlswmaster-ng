/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gamescanner.h"

#define DEFAULT_SCAN_INTERVAL 30

GameScanner::GameScanner(Config& conf, ModuleList& modList)
: modList(modList)
{
	msock = new MultiSock(conf);

	int interval = conf.getInteger("global", "scan_interval", DEFAULT_SCAN_INTERVAL);
	TimerService::registerTimer(new Timer(new ScanEvent(*this), interval));
}

GameScanner::~GameScanner()
{
	while (!pktList.isEmpty())
		delete pktList.get();

	delete msock;
}

int GameScanner::execute(void* arg)
{
	while (1) {
		NetPkt* pkt = msock->recv();
		if (pkt != NULL) {
			pktList.add(pkt);
			pktCount.post();
		}
	}
	return 0;
}

void GameScanner::scan()
{
	modList.scan(msock);
}

NetPkt* GameScanner::getPkt()
{
	pktCount.wait();
	return pktList.get();
}
