/***************************************************************************
 *   Copyright (C) 04/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <string.h>
#include "mod_d3engine.h"
#include "modhelper.h"

static struct game_ports port_arr[] = {
	{ 27666, 27673, ID_D3 }, // Doom 3
	{ 28004, 28008, ID_Q4 }, // Quake 4
	{ 0,0,0 }
};

static const char scanmsg[] = "\xff\xffgetInfo";
static const char replyhead[] = "\xff\xffinfoResponse";

void ModD3Engine::scan(MultiSock* msock)
{
	ModHelper::send(msock, port_arr, scanmsg, strlen(scanmsg));
}

int ModD3Engine::parse(NetPkt* pkt, GameList* glist)
{
	int gameid;

	gameid = ModHelper::checkPorts(pkt, port_arr);
	if (!gameid)
		return PARSE_REJECT;

	if (!pkt->compare(0, replyhead, strlen(replyhead)))
		return PARSE_REJECT;

	glist->addGame(gameid, pkt);
	return PARSE_ACCEPT;
}
